let paymentComplete = false;
let paymentX, paymentY;
let slider;


function preload() {
    googleImg = loadImage("google2.jpg");
    searchImg = loadImage("searchImg.jpg");
    transparentImg = loadImage("transparent.jpg");
    commercial1 = loadImage("1.gif");
    commercial2 = loadImage("2.gif");
    commercial3 = loadImage("3.gif");
    commercial4 = loadImage("4.gif");
    commercial5 = loadImage("5.gif");
    commercial6 = loadImage("6.gif");
}

function setup() {
    createCanvas(940, 680); // Setup canvas
    createVideoCapture(); // Call function
    createSearchbar(); // Call function

    //Slider
    slider = createSlider(0, 255, 255);
    slider.position(390, 450);

}

function createVideoCapture() {
    videoCapture = createCapture(VIDEO);
    videoCapture.size(940, 680); // Set capture size
    videoCapture.hide(); // Hide DOM element to call other stuff (otherwise it would be shown both under the canvas and on)
}

function createSearchbar() {
    let barWidth = 350;
    let barHeight = 50;

    let xBar = 250;
    let yBar = height - 300;

    // Create empty input
    input = createInput('');
    input.position(xBar, yBar);
    input.size(barWidth, barHeight);
}

function draw() {

    image(videoCapture, 0, 0, width, height); // Fit video to canvas
    filter(INVERT); // Invert colors 


    // Commercial 1,2,3,4,5,6 loaded + position + text
    push();
    tint(255, 127);
    fill(0);
    image(commercial1, 20, 20, commercial1.width, commercial1.height);
    text("processing data...", 20, 20 + commercial1.height + 20); // Add text below commercial1
    image(commercial3, 580, 10, commercial3.width * 0.75, commercial3.height * 0.75);
    text("processing data...", 670, 10 + (commercial3.height * 0.75) + 20); // Add text below commercial3
    image(commercial4, 5, 250, commercial4.width, commercial4.height);
    text("processing data...", 5, 250 + commercial4.height + 20); // Add text below commercial4
    image(commercial2, 200, 500, commercial2.width, commercial2.height);
    text("processing data...", 200, 500 + commercial2.height + 20); // Add text below commercial2
    image(commercial5, 500, 350, commercial5.width, commercial5.height);
    text("processing data...", 500, 350 + commercial5.height + 20); // Add text below commercial5
    image(commercial6, 420, 100, commercial6.width * 0.4, commercial6.height * 0.4);
    text("processing data...", 420, 100 + (commercial6.height * 0.4) + 20); // Add text below commercial6
    pop();


    push();
    if (paymentComplete) {
        fill(57, 255, 20);
        textFont("Courier New", 50, 100);
        text("💲Payment Complete💲", paymentX, paymentY);
    }
    pop();

    // White rect that is infront of all the "data"
    // Local variable with slider value at the alpha values position
    
    
    let alphaValue = slider.value();
    fill(255, 255, 255, alphaValue)
    rect(0, 0, width, height);
  

    // GoogleImg position
    push();
    let imgWidth = googleImg.width * 0.28; // Adjusted size 
    let imgHeight = googleImg.height * 0.28; // Adjusted size 
    let xImg = width - 660;
    let yImg = height - 485;

    // Display googleImg 
    image(googleImg, xImg, yImg, imgWidth, imgHeight);
    pop();

    // TransparentImg postion
    push();
    let transWidth = transparentImg.width * 0.32; // Adjusted size 
    let transHeight = transparentImg.height * 0.32; // Adjusted size 
    let xTransImg = width - 640;
    let yTransImg = height - 380;

    // Display transparentImg
    image(transparentImg, xTransImg, yTransImg, transWidth, transHeight);
    pop();

    // Find position for the rectangle to be in the middle
    push();
    fill(228, 246, 245);
    noStroke();
    let rectWidth = 450;
    let rectHeight = 65;
    let xRect = width - 700;
    let yRect = height - 307;
    rect(xRect, yRect, rectWidth, rectHeight, 10);
    pop();

    // Seachbar
    // Set placeholder text & In-Line CSS + HTML 
    push();
    input.attribute('placeholder', 'Search here');
    input.style("padding-left", "20px");
    input.style("border", "blue");
    input.class('search-bar');
    input.style("border-radius", "30px");
    pop();

    // SearchImg position
    push();
    let searchWidth = searchImg.width * 0.08; // Adjusted size 
    let searchHeight = searchImg.height * 0.08; // Adjusted size 
    let xSearch = width - 305;
    let ySearch = height - 295; 

    // Display searchImg
    image(searchImg, xSearch, ySearch, searchWidth, searchHeight);
    pop();

}


function mousePressed() {
    // Calculate the boundaries of the area where the searchImg is located
    let searchImgWidth = searchImg.width * 0.08; // Adjusted size 
    let searchImgHeight = searchImg.height * 0.08; // Adjusted size 
    let xSearch = width - 305;
    let ySearch = height - 295; // Adjusted position
    let xSearchEnd = xSearch + searchImgWidth;
    let ySearchEnd = ySearch + searchImgHeight;

    // Check if the mouse is within the area of the searchImg
    if (mouseX > xSearch && mouseX < xSearchEnd && mouseY > ySearch && mouseY < ySearchEnd) {
        // If so, display "payment complete" at a random position
        paymentComplete = true;
        paymentX = random(100, 810);
        paymentY = random(100, 550);
    }
}
