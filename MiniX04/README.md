# MiniX04 - Google Transparent

Google Transparent is developed as a response to Transmediale's open call “Capture All”. It’s created as an alternative Google search page, that lets the user choose the amount of data transparency wanted when operating the engine. This project in a way works as a browser plugin, revealing (if chosen) the amount of data being processed and captured through your actions. 

![hello](https://gitlab.com/matiasaestheticprogramming/aesthetic-programming/-/raw/main/MiniX04/GoogleTransparent.gif)

[Click here to run the program](https://matiasaestheticprogramming.gitlab.io/aesthetic-programming/MiniX04)


[Click here to read the program](https://gitlab.com/matiasaestheticprogramming/aesthetic-programming/-/blob/main/MiniX04/sketch.js)

### The program

This program uses both the webcam, clm face tracker, and the search bar input. It starts by looking mostly like any other search engine but with a slider underneath the search bar. As the value of this slider is lowered so is the background's alpha value, revealing what is behind. What you will find here are different data being captured, like data from your webcam, the input search bar on so on. 

Personalized data is captured and saved by companies making the “user experience” better but also the targeted advertisement. That’s why you in this project also can find different targeted advertisers processing your data to enhance your “user experience” but of course also their sales. Nothing in this world is free, and if you don’t pay for the product then it's probably because you are the product.  

To create these data capture examples, I have used and learned syntax such as `preload`, `createSlider`, `createCapture(VIDEO)`, `new clm.tracker`, `createInput` and also `push` `pop`- a method to change the original array. 

### Capture All

In the world that we live in today our online appearance leaves behind a great digital trail. A trail like this is worth a lot in the hands of the right people which is also why this topic is so very important. In its simplest ways, data capture is through which our every little click, tap, or scroll is valuable insights for tech giants and later in the process also so many others. We are constantly leaving a digital footprint that forms a precise profile of who we are, and what we like. It doesn’t seem to take long before surveillance capitalists like Google and Meta know us better than we do ourselves. Knowledge that then is sold to earn money, and to make us spend money. 

Graphical user interfaces play an important role in the way that big data and information are presented to the user. Now, more than ever, data is being easily collected but also effortly returned and presented to the user on visually engaging platforms. This can be the great thing about GUIs, but for who? We often see cases of big tech companies right at the edge of GDPR rules regarding data transparency about the sale of your data. It wasn't long ago that Meta received a billion dollar fine for violating EU regulations on personal data. The authority found that a “contract” couldn’t be used as a legal reason to send users targeted advertisements based on their online activity. This is why last November, you had to actively choose whether you would like to pay for Meta’s platforms to avoid advertisement or consent to such activity, paying with your data.  

This topic is indeed broad, and whatever your opinion might be towards the conversation around capture and surveillance capitalism `Google Transparent` hands you the chance to take a glance into the data that is the price of digital appearance, advocating for a more transparent datafication. Or not. Thats your choice.  
  

### References
* Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp 55-70
* p5.js Refrences: https://p5js.org/reference/#/p5/
* Transmediale "Call for works, 2015" CAPTURE ALL: https://archive.transmediale.de/content/call-for-works-2015
* Shoshana Zuboff on surveillance capitalism | VPRO Documentary: https://www.youtube.com/watch?v=hIXhnWUmMvw&ab_channel=vprodocumentary
* META with big EU-privacyfine: https://www.dr.dk/nyheder/indland/nu-kan-du-betale-dig-fra-reklamer-paa-facebook-og-instagram
