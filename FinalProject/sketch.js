// global variables, can be used/manipulated/referenced to throughout the code
let scene = 0;
let circleX = 680; // red dot starting position
let circleY = 580; // ^^
let answerOptionImgWidth = 350; // the answer options image's dimensions
let answerOptionImgHeight = 250;
let answerOptionImgSpacing = 20;
let answerAEffectX = -45; // variables that define the effect the answers have on the red dot/circle
let answerAEffectY = -45; // ^^
let answerBEffectX = 45;
let answerBEffectY = -45;
let answerCEffectX = -45;
let answerCEffectY = 45;
let answerDEffectX = 45;
let answerDEffectY = 45;
let maxprofileTextXPosition = 300; // make sure that the text is confined within a "box" of sorts and isnt one long line
let maxProfileTextHeight = 100; // ^^

let profile = []; // empty array that will store the profile strings obtained after each answer 
let profiletexts = ["The right to make your own choices, no matter the consequences, is a core value to you and important above all else.",
  "Privacy and the right to be your true self is a core value, and important above all else.",
  "A well-functioning society needs strong leaders and strong laws, and is important above all else.",
  "The right to knowledge and your own history is a core value to you, and important above all else.",
  "You lean towards independence and individualism, championing personal autonomy and valuing self-reliance highly.",
  "You advocate for loyalty and traditional values, prioritizing community cohesion and steadfast adherence to established norms.",
  "You gravitate towards introspection and pacifism, emphasizing deep contemplation and a commitment to non-violent conflict resolution.",
  "You are inclined towards prioritizing practicality, minimalism, and utilitarianism in decision-making, focusing on efficiency and rationality above all else.",
  "You prioritize emotional depth and societal reflection, valuing nuanced perspectives and empathy in governance.",
  "You lean towards political values centered around transparency, evidence-based decision-making, and holding authority accountable through rigorous scrutiny and fact-checking.",
  "You lean towards political values that emphasize humor, optimism, and levity, promoting unity through shared laughter and lightheartedness even in challenging times.",
  "You lean towards political values that prioritize strength, decisiveness, and assertiveness, advocating for bold action and a proactive approach to governance and international relations.",
  "You embody values characterized by a direct, no-nonsense approach, aligning with conservative principles of efficiency, transparency, and accountability in governance.",
  "You epitomize values that advocate for moderation, bipartisanship, and pragmatic compromise, aligning with centrist ideals of balance and cooperation.",
  "You embody political values marked by intense focus, determination, and a results-oriented mindset, aligning with libertarian or neoliberal principles of individualism, innovation, and economic growth.",
  "You embody values rooted in alternative thinking, non-conformity, and progressive ideals, aligning with leftist or green politics advocating for systemic change, social justice, and environmental sustainability.",
  "You reflect political values regarding gender and feminism associated with anti patriarchal activism and challenging traditional gender roles, aligning with progressive perspectives that prioritize the right to feel safe in public spaces as well as fighting rape culture.",
  "You, reflect political values regarding gender and feminism associated with traditional gender roles and patriarchal norms, aligning with conservative perspectives on masculinity and femininity.",
  "You embody political values characterized by a non-conformist stance on gender and feminism, challenging traditional gender roles and societal expectations, advocating for gender equality, and questioning the norms that perpetuate gender-based stereotypes and discrimination.",
  "You reflect hyperbolic political values regarding gender and feminism that prioritize the avoidance of violence and conflict, potentially aligning with pacifist perspectives that seek to mitigate harm and promote peace, but not distinctly aligning with specific gender or feminist ideologies."];


class AnswerOption {
  constructor(img, x, y, profiletext, effectX, effectY) { // these are the parameters of the constructor function
    this.img = img; // these are properties of the object instance
    this.x = x;
    this.y = y;
    this.width = answerOptionImgWidth;
    this.height = answerOptionImgHeight;
    this.profiletext = profiletext;  // Store the profile text associated with this option
    this.effectX = effectX; // Effect on circle's X position
    this.effectY = effectY; // Effect on circle's Y position
  }

  display() { // method
    image(this.img, this.x, this.y, this.width, this.height);
  }

  checkClicked() { // method that is later called as a message in the mousePressed function
    if (
      mouseX > this.x &&
      mouseX < this.x + this.width &&
      mouseY > this.y &&
      mouseY < this.y + this.height
    ) {
      scene++;
      profile.push(this.profiletext);
      return true; // Return true when clicked
    }
    return false; // Return false otherwise
  }

  answerEffect() { // method
    // Update the position of the circle based on the effect of this answer option when clicked
    circleX += this.effectX; // += is an addition operator
    circleY += this.effectY;
  }

}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(235);

  let centerX = windowWidth / 2; // our personal center to fit the background image
  let centerY = windowHeight / 2 + 130; // ^^

  let optionAx = centerX - answerOptionImgWidth - answerOptionImgSpacing;
  let optionAy = centerY - answerOptionImgHeight - answerOptionImgSpacing;
  let optionBx = centerX + answerOptionImgSpacing;
  let optionBy = centerY - answerOptionImgHeight - answerOptionImgSpacing;
  let optionCx = centerX - answerOptionImgWidth - answerOptionImgSpacing;
  let optionCy = centerY + answerOptionImgSpacing;
  let optionDx = centerX + answerOptionImgSpacing;
  let optionDy = centerY + answerOptionImgSpacing;

  optionAfly = new AnswerOption(fly, optionAx, optionAy, profiletexts[0], answerAEffectX, answerAEffectY); // for the profile texts we could have made an additional class to hold those instances
  optionBinvisibility = new AnswerOption(invisibility, optionBx, optionBy, profiletexts[1], answerBEffectX, answerBEffectY);
  optionCstrength = new AnswerOption(strength, optionCx, optionCy, profiletexts[2], answerCEffectX, answerCEffectY);
  optionDtimetravel = new AnswerOption(timetravel, optionDx, optionDy, profiletexts[3], answerDEffectX, answerDEffectY);
  optionAcat = new AnswerOption(cat, optionAx, optionAy, profiletexts[4], answerAEffectX, answerAEffectY);
  optionBdog = new AnswerOption(dog, optionBx, optionBy, profiletexts[5], answerBEffectX, answerBEffectY);
  optionCfish = new AnswerOption(fish, optionCx, optionCy, profiletexts[6], answerCEffectX, answerCEffectY);
  optionDnoPets = new AnswerOption(noPets, optionDx, optionDy, profiletexts[7], answerDEffectX, answerDEffectY);
  optionAdrama = new AnswerOption(drama, optionAx, optionAy, profiletexts[8], answerAEffectX, answerAEffectY);
  optionBdocumentery = new AnswerOption(documentary, optionBx, optionBy, profiletexts[9], answerBEffectX, answerBEffectY);
  optionCcomedy = new AnswerOption(comedy, optionCx, optionCy, profiletexts[10], answerCEffectX, answerCEffectY);
  optionDaction = new AnswerOption(action, optionDx, optionDy, profiletexts[11], answerDEffectX, answerDEffectY);
  optionAblackCoffee = new AnswerOption(blackCoffee, optionAx, optionAy, profiletexts[12], answerAEffectX, answerAEffectY);
  optionBcaffelatte = new AnswerOption(caffelatte, optionBx, optionBy, profiletexts[13], answerBEffectX, answerBEffectY);
  optionCespresso = new AnswerOption(espresso, optionCx, optionCy, profiletexts[14], answerCEffectX, answerCEffectY);
  optionDnoCoffee = new AnswerOption(noCoffee, optionDx, optionDy, profiletexts[15], answerDEffectX, answerDEffectY);
  optionAbear = new AnswerOption(bear, optionAx, optionAy, profiletexts[16], answerAEffectX, answerAEffectY);
  optionBman = new AnswerOption(man, optionBx, optionBy, profiletexts[17], answerBEffectX, answerBEffectY);
  optionCconfused = new AnswerOption(confused, optionCx, optionCy, profiletexts[18], answerCEffectX, answerCEffectY);
  optionDdead = new AnswerOption(dead, optionDx, optionDy, profiletexts[19], answerDEffectX, answerDEffectY);

}

function draw() {

  print(scene) // debugging mechanism

  if (scene === 0) {
    drawScene0();
  } else if (scene === 1) {
    drawScene1();
  } else if (scene === 2) {
    drawScene2();
  } else if (scene === 3) {
    drawScene3();
  } else if (scene === 4) {
    drawScene4();
  } else if (scene === 5) {
    drawScene5();
  } else if (scene === 6) {
    endScene();
  }
}

function keyPressed() { // predefined function in p5
  if (keyCode === ENTER) {
    // Change the scene
    if (scene === 0) {
      scene++;
    }
  }
}

function isMouseOverImage(x, y) {
  return mouseX > x && mouseX < x + answerOptionImgWidth && mouseY > y && mouseY < y + answerOptionImgHeight;
}

function drawFrame(x, y) { // could potentially have been implemented as a method instead!! to avoid repetition
  push(); // push and pop ensure that this doesn't affect all aspects of the code like the profile texts and the questions
  noFill();
  stroke(0);
  strokeWeight(4);
  rect(x, y, answerOptionImgWidth, answerOptionImgHeight);
  pop();
}

function drawScene0() {
  background(backgroundImg);

  // Display question and answer options
  textSize(45);
  textAlign(CENTER, CENTER);
  textFont(font1);
  text("Get to know your true self with this Quiz!", width / 2, height / 2.8);
  textSize(map(sin(frameCount * 0.1), -2, 2, 35, 43)); // makes the text pulsate, 35 is the minimum font size, 43 is the max font size - hierarchy of functions
  textFont(font2);
  text("Press ENTER to continue", width / 2, height / 2.3);

}

function drawScene1() {
  clear();
  background(backgroundImg);

  // Display question and answer options
  optionAfly.display(); // message being used, not a method
  optionBinvisibility.display();
  optionCstrength.display();
  optionDtimetravel.display();

  textSize(50);
  textAlign(CENTER, CENTER);
  textFont(font1);
  text("Question 1", width / 2, height / 5);
  textSize(32);
  textFont(font2);
  text("If you could choose a superpower what would it be?", width / 2, height / 3.5);

  // Check if the mouse is over any of the answer options
  if (isMouseOverImage(optionAfly.x, optionAfly.y)) { // question 1's answer options are used for every scene since all answer options are consistent in placement
    drawFrame(optionAfly.x, optionAfly.y); // this gives the arguments to the function drawFrame rectangle
  } else if (isMouseOverImage(optionBinvisibility.x, optionBinvisibility.y)) { 
    drawFrame(optionBinvisibility.x, optionBinvisibility.y);
  } else if (isMouseOverImage(optionCstrength.x, optionCstrength.y)) {
    drawFrame(optionCstrength.x, optionCstrength.y);
  } else if (isMouseOverImage(optionDtimetravel.x, optionDtimetravel.y)) {
    drawFrame(optionDtimetravel.x, optionDtimetravel.y);
  }
}

function drawScene2() {
  clear();
  background(backgroundImg);

  // Display question and answer options
  optionAcat.display();
  optionBdog.display();
  optionCfish.display();
  optionDnoPets.display();

  textSize(50);
  textAlign(CENTER, CENTER);
  textFont(font1);
  text("Question 2", width / 2, height / 5);
  textSize(32);
  textFont(font2);
  text("Choose a pet to take home with you", width / 2, height / 3.5);

  if (isMouseOverImage(optionAfly.x, optionAfly.y)) {
    drawFrame(optionAfly.x, optionAfly.y);
  } else if (isMouseOverImage(optionBinvisibility.x, optionBinvisibility.y)) {
    drawFrame(optionBinvisibility.x, optionBinvisibility.y);
  } else if (isMouseOverImage(optionCstrength.x, optionCstrength.y)) {
    drawFrame(optionCstrength.x, optionCstrength.y);
  } else if (isMouseOverImage(optionDtimetravel.x, optionDtimetravel.y)) {
    drawFrame(optionDtimetravel.x, optionDtimetravel.y);
  }
}

function drawScene3() {
  clear();
  background(backgroundImg);

  // Display question and answer options
  optionAdrama.display();
  optionBdocumentery.display();
  optionCcomedy.display();
  optionDaction.display();

  textSize(50);
  textAlign(CENTER, CENTER);
  textFont(font1);
  text("Question 3", width / 2, height / 5);
  textSize(32);
  textFont(font2);
  text("Select your preferred movie genre", width / 2, height / 3.5);

  if (isMouseOverImage(optionAfly.x, optionAfly.y)) {
    drawFrame(optionAfly.x, optionAfly.y);
  } else if (isMouseOverImage(optionBinvisibility.x, optionBinvisibility.y)) {
    drawFrame(optionBinvisibility.x, optionBinvisibility.y);
  } else if (isMouseOverImage(optionCstrength.x, optionCstrength.y)) {
    drawFrame(optionCstrength.x, optionCstrength.y);
  } else if (isMouseOverImage(optionDtimetravel.x, optionDtimetravel.y)) {
    drawFrame(optionDtimetravel.x, optionDtimetravel.y);
  }
}

function drawScene4() {
  clear();
  background(backgroundImg);

  // Display question and answer options
  optionAblackCoffee.display();
  optionBcaffelatte.display();
  optionCespresso.display();
  optionDnoCoffee.display();

  textSize(50);
  textAlign(CENTER, CENTER);
  textFont(font1);
  text("Question 4", width / 2, height / 5);
  textSize(32);
  textFont(font2);
  text("Choose your favorite type of coffee", width / 2, height / 3.5);

  if (isMouseOverImage(optionAfly.x, optionAfly.y)) {
    drawFrame(optionAfly.x, optionAfly.y);
  } else if (isMouseOverImage(optionBinvisibility.x, optionBinvisibility.y)) {
    drawFrame(optionBinvisibility.x, optionBinvisibility.y);
  } else if (isMouseOverImage(optionCstrength.x, optionCstrength.y)) {
    drawFrame(optionCstrength.x, optionCstrength.y);
  } else if (isMouseOverImage(optionDtimetravel.x, optionDtimetravel.y)) {
    drawFrame(optionDtimetravel.x, optionDtimetravel.y);
  }
}

function drawScene5() {
  clear();
  background(backgroundImg);

  // Display question and answer options
  optionAbear.display();
  optionBman.display();
  optionCconfused.display();
  optionDdead.display();

  textSize(50);
  textAlign(CENTER, CENTER);
  textFont(font1);
  text("Question 5", width / 2, height / 5);
  textSize(32);
  textFont(font2);
  text("Who would you prefer to be with if you were alone in a forest?", width / 2, height / 3.5);

  if (isMouseOverImage(optionAfly.x, optionAfly.y)) {
    drawFrame(optionAfly.x, optionAfly.y);
  } else if (isMouseOverImage(optionBinvisibility.x, optionBinvisibility.y)) {
    drawFrame(optionBinvisibility.x, optionBinvisibility.y);
  } else if (isMouseOverImage(optionCstrength.x, optionCstrength.y)) {
    drawFrame(optionCstrength.x, optionCstrength.y);
  } else if (isMouseOverImage(optionDtimetravel.x, optionDtimetravel.y)) {
    drawFrame(optionDtimetravel.x, optionDtimetravel.y);
  }
}

function endScene() {
  clear();
  background(backgroundImg);

  textSize(50);
  textAlign(CENTER, CENTER);
  textFont(font1);
  text("Quiz Results", width / 2, height / 6.5);
  textSize(32);
  textFont(font2);
  text("Nice! Your political alignment quiz results are in!", width / 2, height / 4.8);

  graphImgWidth = 500;
  graphImgHeight = 500;

  // Calculate the coordinates to place the image in the center of the canvas
  let graphImgX = (width - graphImgWidth) / 3;
  let graphImgY = (height - graphImgHeight) / 1.5;

  image(emptyGraphImg, graphImgX, graphImgY, graphImgWidth, graphImgHeight);

  // Show ellipse end result
  push();
  fill(255, 0, 0);
  ellipse(circleX, circleY, 20);
  pop();

  fill(0)
  let profileText1 = profile[0];
  let profileText2 = profile[1];
  let profileText3 = profile[2];
  let profileText4 = profile[3];
  let profileText5 = profile[4];

  textSize(15);
  textAlign(CENTER, CENTER);
  textWrap(WORD);

  let profileTextXPosition = width / 1.8; // where on the canvas in terms of X coordinate the profile texts should be

  let profileTextAnswerOptionImgSpacing = height / 10;

  // Display profile end result
  text(profileText1, profileTextXPosition, height / 3.5, maxprofileTextXPosition, maxProfileTextHeight);
  text(profileText2, profileTextXPosition, height / 3.5 + profileTextAnswerOptionImgSpacing, maxprofileTextXPosition, maxProfileTextHeight);
  text(profileText3, profileTextXPosition, height / 3.5 + 2 * profileTextAnswerOptionImgSpacing, maxprofileTextXPosition, maxProfileTextHeight);
  text(profileText4, profileTextXPosition, height / 3.5 + 3 * profileTextAnswerOptionImgSpacing, maxprofileTextXPosition, maxProfileTextHeight);
  text(profileText5, profileTextXPosition, height / 3.5 + 4 * profileTextAnswerOptionImgSpacing, maxprofileTextXPosition, maxProfileTextHeight);

}



function mousePressed() {
  if (scene === 1) {
    if (optionAfly.checkClicked()) { // message
      optionAfly.answerEffect(); // message
    }
    if (optionBinvisibility.checkClicked()) {
      optionBinvisibility.answerEffect();
    }
    if (optionCstrength.checkClicked()) {
      optionCstrength.answerEffect();
    }
    if (optionDtimetravel.checkClicked()) {
      optionDtimetravel.answerEffect();
    }
  } else if (scene === 2) {
    if (optionAcat.checkClicked()) {
      optionAcat.answerEffect();
    }
    if (optionBdog.checkClicked()) {
      optionBdog.answerEffect();
    }
    if (optionCfish.checkClicked()) {
      optionCfish.answerEffect();
    }
    if (optionDnoPets.checkClicked()) {
      optionDnoPets.answerEffect();
    }
  } else if (scene === 3) {
    if (optionAdrama.checkClicked()) {
      optionAdrama.answerEffect();
    }
    if (optionBdocumentery.checkClicked()) {
      optionBdocumentery.answerEffect();
    }
    if (optionCcomedy.checkClicked()) {
      optionCcomedy.answerEffect();
    }
    if (optionDaction.checkClicked()) {
      optionDaction.answerEffect();
    }
  } else if (scene === 4) {
    if (optionAblackCoffee.checkClicked()) {
      optionAblackCoffee.answerEffect();
    }
    if (optionBcaffelatte.checkClicked()) {
      optionBcaffelatte.answerEffect();
    }
    if (optionCespresso.checkClicked()) {
      optionCespresso.answerEffect();
    }
    if (optionDnoCoffee.checkClicked()) {
      optionDnoCoffee.answerEffect();
    }
  } else if (scene === 5) {
    if (optionAbear.checkClicked()); {
      optionAbear.answerEffect();
    }
    if (optionBman.checkClicked()) {
      optionBbman.answerEffect();
    }
    if (optionCconfused.checkClicked()) {
      optionCconfused.answerEffect();
    }
    if (optionDdead.checkClicked()) {
      optionDdead.answerEffect();
    }
  }
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}