# MiniX01 

![hello](https://gitlab.com/matiasaestheticprogramming/aesthetic-programming/-/raw/main/MiniX01/Screenshot_2024-03-02_at_11.28.40.png)

[Click here to run the program](https://matiasaestheticprogramming.gitlab.io/aesthetic-programming/MiniX01)

[Click here to read the program](https://gitlab.com/matiasaestheticprogramming/aesthetic-programming/-/blob/e5f368344dec854fa6d9ae55803bb6f5979fde58/MiniX01/sketch.js)

### What have I produced? 
My program begins with creating the fundamental canvas that is a necessity to begin drawing or writing code in p5.js. The value can really be anything inside the boundaries of an HTML site. What is particularly interesting about my code is the use of the numeric variables `windowWidth` and `windowHeight`, which was my first meeting with p5.js as the JavaScript library it is. This, along with most of the code used in my `MiniX01`, was discovered through the reference list on their website. 

Next, I stumbled upon the `random()` function, and I instantly knew that I wanted to create something with it. Here I started defining some different variables `let x, y, r, g, b;` and gave them the function random. With the function `draw()` in p5.js continuously executing the line of code I was able to create an animation of random circles, with random positions and RGB values. 

At last, I knew that I wanted to be able to loop something since I find this a powerful syntax/tool looking into future programs. Therefore I created a `for-loop` where I want the `variable y` to increase by 100 and continue looping as long as y is less than height. I also used different constants like `text` and `BLUR` and the method `alpha`, but more on that in the next paragraphs. 

### My first independent p5.js coding experience 
Being my first real independent coding experience with library p5.js I must say that I am very impressed and thrilled about the accessibility of different creative tools. Before attending this class I had absolutely no experience with coding, and really no sense of what it was. And if you had told me just a week ago that I this fast would be able to generate motion and a generative reflection piece, I wouldn’t believe you. This is exactly what I think aesthetic programming is all about, and the reason for its creation. The lack of diversity in coding or programming environments in a world dominated mainly by white binary men, thus its importance in modern society. `"to program or be programmed”`. 

### Reflections
Something that started rather `random()` received a bigger meaning. At least for me, as I began to see blurry blinding traffic lights in all of the colored blurry circles. This made me think of the significant increase in young people driving under the influence of alcohol. In 2019 it was 9%, and in 2022 the number was 15%, and the increase in circles for every time the code is run, I think could be a symbol for this. This horrible topic, like so many others is man-dominated, as nine out of ten drunk drivers are men. So maybe next time you `start…, consider to stop…`

### References
* Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48
* Sikker Trafik - https://sikkertrafik.dk/rad-og-viden/bil/spirituskorsel/
* Reference p5.js - https://p5js.org/reference/#/p5/

