function setup() {
  // put setup code here
  // runs only once when the program starts
  createCanvas(windowWidth, windowHeight);
  background(0);
  
}

function draw() {
  // put drawing code here
  // the function random() is called with arguments 
  // arguments: 500...width...

  //variabler 
  let x, y, r, g, b;

  r = random(500);
  g = random(500)
  b = 0;
  x = random(width);
  y = random(height);
  noStroke();
  fill(r, g, b, 100);
  circle(x, y, 100);

  // for loop
  for (let y = 50; y <= height; y = y + 100) {
    textSize(32);
    fill(255);
    stroke(100);
    strokeWeight(4);
    text("START?...STOP?", 50, y);
    filter(BLUR, 1)
  }

  //textSize(25)
  //textStyle(ITALIC);
  //fill("red")
  //text('"Every fifth fatality in traffic accidents dies in a drunk driving accident"', 500, 100, 300, 300);
  //textSize(18)
  //text('"There are many reasons for drunk driving. Nine out of ten drunk drivers are men"', 650, 350, 300, 300);
  //textSize(30)
  //text('"In 2019, 9% of young drivers said they had driven under the influence of alcohol. In 2022, the number was 15%"', 500, 600, 300, 300);
  
}

