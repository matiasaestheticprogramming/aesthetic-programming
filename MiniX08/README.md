# MiniX08 - "DogPark API" 
![hello](https://gitlab.com/matiasaestheticprogramming/aesthetic-programming/-/raw/main/MiniX08/dogpark.png) 

Welcome to the Dog Park is an interactive application designed to entertain users by generating random dog images and names on a virtual dog park backdrop. Each time a user clicks within the app window, a new dog image appears at the click location along with a randomly selected dog name displayed below the image. This project utilizes p5.js for drawing and interaction, the Dog CEO's Dog API for fetching random dog images, and custom sound effects to enhance the user experience.

[Click here to run the program](https://matiasaestheticprogramming.gitlab.io/aesthetic-programming/MiniX08)

[Click here to read the program](https://gitlab.com/matiasaestheticprogramming/aesthetic-programming/-/blob/main/MiniX08/dogApi.js)

## API 
* Dog CEO's Dog API: Provides random images of dogs. This API was chosen for its simplicity and reliability, allowing for easy integration into our interactive setup. -> https://dog.ceo/dog-api/

## Reflection on the Process

The main data for this project comes from the Dog CEO's Dog API, which is utilized to fetch random dog images. The process involves sending HTTP GET requests to retrieve data in JSON format, specifically the URL of a random dog image. This simplicity in data structure (URLs pointing to images) makes it easy to handle and integrate directly into our visual setup.

The data, while simple, is effectively represented visually in a playful and interactive manner. Images are not just displayed but made a part of an engaging digital environment, complemented by corresponding names from a predefined list and thematic audio elements.

The Dog API, like many public APIs, abstracts much of the data processing and storage, delivering only the final data needed (image URLs). It's an example of how APIs can streamline app development by handling complex backend processes and allowing developers to focus on frontend experience. However, the sorting and management of this data on the API provider's server remain opaque.

## Power Relations and API Significance
APIs like Dog CEO represent a power dynamic where the provider controls data access, structure, and reliability. They play a crucial role in digital culture by encapsulating functionalities and data, promoting modularity, and enabling diverse applications to be built with rich feature sets that would be hard to develop independently. 

If time permitted, we would like to explore the following questions further:
* How do API providers manage and prioritize traffic and requests to ensure service reliability and fairness?

* What are the challenges in maintaining an API from the provider's perspective, especially regarding data consistency, scaling, and security?

* How can developers handle and visualize data more dynamically from multiple APIs to create more complex and data-rich applications?

This project not only showcases a fun and interactive use of web APIs but also invites users to consider the underlying technology and the data it manipulates. It opens up avenues for discussing the importance of APIs in modern digital infrastructures and the hidden complexities of seemingly simple applications.
