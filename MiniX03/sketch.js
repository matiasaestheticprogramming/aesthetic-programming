let browImg;

function preload() {
  browImg = loadImage("blankP.jpg"); // Preload the image
  cursorImg = loadImage("mac-cursor-6.jpg")
}

function setup() {
  createCanvas(windowWidth, windowHeight); // Drawing Canvas
  frameRate(10); // Frame rate parameter
  textFont("Courier New", 10);
}

function draw() {
  // Image dimensions
  let imgWidth = 960;
  let imgHeight = 580;

  // Calculating the top-left corner positions to center the image
  let x = (windowWidth - imgWidth) / 2;
  let y = (windowHeight - imgHeight) / 2;

  // Draw the image at calculated position
  image(browImg, x, y, imgWidth, imgHeight); 

  tint("red")

  let x2 = (windowWidth - imgWidth) / 2;
  let y2 = (windowHeight - imgHeight) / 2;

  // Draw the image at calculated position
  image(cursorImg, x, y, imgWidth, imgHeight); 

  // Background color
  let r = random(500);
  let g = random(500);
  let b = 0;
  background(r, 255, 225, 20); // Background with alpha value

  // Draw ellipse in the middle of the page
  fill(255); // White color
  noStroke()
  ellipse(windowWidth / 2, windowHeight / 2, 225, 225); // Ellipse at the center
  fill(0)
  ellipse(windowWidth / 2, windowHeight / 2, 20, 20); // Ellipse at the center
  
  text("Be patient", windowWidth / 2, windowHeight / 2 - 170);

  // Text "Place your cursor in the middle to continue processing"
  text("Place your cursor in the middle to continue processing", windowWidth / 2, windowHeight / 2 + 200);
  textSize(28)
  textAlign(CENTER)

  // Throbber
  let num = 20;
  push();
  
  // Move things to the center
  translate(mouseX, mouseY);
  
  // 360/num >> degree of each ellipse's movement;
  // frameCount%num >> get the remainder that to know which one
  // among 8 possible positions.
  let cir = 360 / num * (frameCount % num);
  rotate(radians(cir));
  noStroke();
  fill(0); // Black color
  
  // The x parameter is the ellipse's distance from the center
  textSize(13)
  text("processing", 25, 0, 100, 100);
  pop();

}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function resetSketch() {
}

