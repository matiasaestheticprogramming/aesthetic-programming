# MiniX03 - Processing...
![hello](https://gitlab.com/matiasaestheticprogramming/aesthetic-programming/-/raw/main/MiniX03/MiniX0.gif?ref_type=heads)

[Click here to run the program](https://matiasaestheticprogramming.gitlab.io/aesthetic-programming/MiniX03)

[Click here to read the program](https://gitlab.com/matiasaestheticprogramming/aesthetic-programming/-/blob/main/MiniX03/sketch.js)

### What have I used and learnt? 
In my `MiniX03` I think that I have managed to connect already-learned tasks with new p5js discoveries. This week we have learned about `for-loops` which I have experimented with in my MiniX0, but also the `push()` and `pop()` functions. Meant to save the current style and restore setting respectively. This tool is especially effective when saving and restoring styles. In my code however because of the image, push and pop don’t have an effect, since the arrangement of the code already makes it work. But now I have learned about it, and its possibilities. 

Other than that I have also played with new different and funny p5js library functions such as `tint()`, `setup()`, `frameRate()`, `windowResized()`, `resetSketch()`, `translate()` 


### When it's time for the machine to take a breather
I got pretty excited when I found out our task this week was to come up with our own throbber design. A specific topic quickly came to mind, because of my own experience and feelings towards the topic, since I’ve like so many others have felt that frustration when something doesn't work as swiftly as you expect it to. Especially with technology being this good as it is today, we are raised in a technological modernization expecting it always to work efficiently and fast. Slow loading times just don't cut it anymore, even if we technically have a moment to spare.

Many of today's systems aim to always deliver instant feedback, helping the user to bridge the gulf of execution in the form of a throbber. Telling us, the user, that the conceptual model is understood correctly, that the computer is thinking, and therefore also the graphical user interface. Even though most of us know that this is the case, we still find ourselves tapping and clicking around, closing browsers, or even closing the computer not tolerating the behavior of the machine. Some would even say that the user often forgets and underappreciates the work behind what they watch on their screens. 

With that in mind, I have with my MiniX03 designed a throbber that does not tolerate your tapping or clicking around. Forcing the user to be patient, since it only works properly when placed in the middle of the loading screen, insisting on a pause, and serving for a better relationship between the user, hardware, and software.
 
### References
* Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp 55-70
* p5.js Refrences: https://p5js.org/reference/#/p5/
* bookThrobberSketch lektion 9: https://brightspace.au.dk/d2l/le/lessons/123559/topics/1793620

