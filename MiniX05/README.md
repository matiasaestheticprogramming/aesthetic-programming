# MiniX05 - Auto-generator
![hello](https://gitlab.com/matiasaestheticprogramming/aesthetic-programming/-/raw/main/MiniX05/miniX05-1.gif) 
![hellu](https://gitlab.com/matiasaestheticprogramming/aesthetic-programming/-/raw/main/MiniX05/minix05-2.gif)

Introducing the concepts of input and output, my generative art piece implements simple rules to reflect upon the idea of auto-generative art conceptually. The code generates a dynamic, animated visual with various colorful circles that live across the canvas. 

[Click here to run the program](https://matiasaestheticprogramming.gitlab.io/aesthetic-programming/MiniX05)


[Click here to read the program](https://gitlab.com/matiasaestheticprogramming/aesthetic-programming/-/blob/main/MiniX05/sketch.js)


## The rules for this auto-generative program are:
* **Draw circles with a random color & size**
* **Update positions of the circles based on noise**
* **Generate a random amount of circles between (10, 800)**


This week MiniX05 has challenged my knowledge and understanding of how arrays and for-loops work. When choosing these 3 rules there are a lot of different values for each of the many circles to account for. That is why I have created arrays for the different values to hold multiple attributes like position, color, and size. 
I think that these different rules have resulted in a fun and different take on what an auto-generator can be. I find it pretty and mesmerizing to look at, and if not, you can also refresh the page and let the computer once again generate with the rules in mind. 


The two `for-loops` in the code are also worth mentioning because they serve two important but different tasks. The for-loop in `setup()` runs once to initialize each circle and its attributes. The for-loop in `draw()` however is continuously looping with `noise` making the smooth animation possible. I like this structure and way of separating elements and dynamic behavior. 

## The role of rules and processes
When reading this week's chapter on computational instructions and what they produce, a good place to start might be to understand Allan Turing's creation of the Turing machine, which is one of the first complex creations of rules by a self-operating machine. With a set of complex instructions, it was able to read, write move left, move right, change state, and stop. 

Very relevant to this class this topic also takes the view on generative art. Art, where the artist uses a system such as a programming language to create something based on a set of instructions, and what I here find particularly interesting, is to discuss who the artist then is. Is it the computer, or the person behind it? Initially, I questioned how the computer even could be considered an artist. But then the book compares coding instructions to knitting with the use of a recipe, and I like this comparison, it puts the topic in another context. Try to consider who you think the artist or creator behind a knitted sweater is. Is it the one behind the recipe, or the one knitting it? 

In my opinion and experience working in this way isn't done by one or the other, it is done in collaboration between the one controlling the rules/instruction and the one executing. There is however a shift in the power dynamics between this collaboration. One is setting the scheme and boundaries of what is possible, and the other executes under these circumstances generating a collaborative art piece.   

## References 
* 10 PRINT - https://10print.org/
* Daniel Shiffman "2D arrays" - https://www.youtube.com/watch?v=OTNpiLUSiB4&ab_channel=TheCodingTrain
* Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, Chapter 5. Auto-generator. 
* "Ten Questions Concerning Generative Computer Art" - https://www-jstor-org.ez.statsbiblioteket.dk:12048/stable/43834149?sid=primo&seq=2
