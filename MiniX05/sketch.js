// Arrays to hold noise offset values, colors, and sizes for each circle
let xOffsets = [];
let yOffsets = [];
let colors = [];
let sizes = [];

function setup() {
    createCanvas(800, 800);
    numCircles = random(10, 800);

    for (let i = 0; i < numCircles; i++) {
        // Initialize noise offsets for each circle
        xOffsets[i] = random(800);
        yOffsets[i] = random(800);

        // Initialize color and size for each circle
        colors[i] = [random(255), random(255), random(255)];
        sizes[i] = random(10, 35);
    }
}

function draw() {
    background(232, 220, 202);
    frameRate(50);

    for (let i = 0; i < numCircles; i++) {
        // Compute positions based on noise
        let x = noise(xOffsets[i]) * width;
        let y = noise(yOffsets[i]) * height;

        // Increment noise offsets for smooth, continuous movement
        xOffsets[i] += 0.002;
        yOffsets[i] += 0.002;

        // If the circle is on the right half of the canvas
        if (x > width / 2) {
            fill(255);
        } else {
            // Set fill color for each circle
            fill(colors[i][0], colors[i][1], colors[i][2]);
        }

        noStroke(); // Remove stroke for aesthetic reasons

        // Draw circle at updated position with its designated size
        ellipse(x, y, sizes[i], sizes[i]);
    }
}









