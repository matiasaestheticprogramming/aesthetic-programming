# MiniX06 - Games With Objects "America F*ck Yeah" 
![hello](https://gitlab.com/matiasaestheticprogramming/aesthetic-programming/-/raw/main/MiniX06/MiniX06.gif) 

America F.uck Yeah" is a patriotic-themed game made to over-dramatize the Americanisation of symbols. Like the American Bald Eagle, which in my game instead of being placed in front of an American flag as a symbol of freedom tries to navigate around a field of stars from the American flag, resulting in a game over if one is hit. This is all accompanied by the thematic song “America F.ck Yeah”. 

[Click here to run the program](https://matiasaestheticprogramming.gitlab.io/aesthetic-programming/MiniX06)


[Click here to read the program](https://gitlab.com/matiasaestheticprogramming/aesthetic-programming/-/blob/main/MiniX06/sketch.js)

## Object-Oriented Programming 
The game leverages Object-Oriented Programming (OOP) principles to manage its complexity and enhance code reusability. Two primary classes, Eagle and Star, form the core of the game mechanics.

**Class Eagle:**
Represents the player's character. It has properties for size, position, and velocity, and methods for movement (jump, move), displaying the eagle (show), and detecting collisions with stars (hits). The eagle reacts to gravity and player input (space bar to jump), providing the game's interactive element.

**Class Star:** 
Represents obstacles in the game. Each star has a random initial position and a fixed size. The stars move horizontally across the screen, and their primary behaviors are encapsulated in the move and show methods.

**Initialization:** 
In the preload function, the game loads external assets such as images and sounds, which are used throughout the game.

**Setup and Input:** 
The setup function initializes the game environment and objects, starting the background music and creating an instance of Eagle. Player input is handled in the keyPressed function, where the eagle jumps in response to the space bar unless the game is over.

**Game Loop:**
The draw function contains the game loop, where game logic is executed continuously. It checks for game-over conditions, updates and renders objects, and handles the spawning of new stars based on a probability, which increases as more stars are on the screen.

**Collision and Game Over:**
Collision detection between the eagle and stars determines if the game ends, stopping the music and displaying a game-over image.

**Audio and Visual Feedback:**
The game provides real-time feedback through sound effects (eagle's flap and background music) and visual cues (dynamic text instructions and game over screen), enhancing the interactive experience.

## Cultural Context 
"America F*uck Yeah" is a game that uses very American symbols like the eagle and the stars, wrapped in a theme that's a playful jab at American culture. It's a small window into how Americans see themselves or maybe how we see Americans but also how they use humor and pride to express national identity.

The game also takes a stand in how American and Western entertainment is shared around the world through digital mediums. This not only spreads American imagery and ideas but also starts conversations about America's influence globally. How people from other countries see the game might vary—they might see it as just fun, or they might see it as a comment on how America interacts with the rest of the world.

"America F*uck Yeah" is a game that uses iconic American symbols, to engage with ideas of freedom and the American spirit. At the same time, it also pokes fun at these very ideas, inviting players to think about American culture in a broader sense. This game reflects the complex feelings Americans have about their own culture—proud, critical, and always ready to laugh at themselves.

## References 
* Daniel Shiffman "Chrome Dinosaur Game" - https://www.youtube.com/watch?v=l0HoJHc-63Q&t=1430s&ab_channel=TheCodingTrain
* Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, Chapter 6. Auto-generator. 
