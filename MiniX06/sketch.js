let eagle;
let eagleImg;
let starImg;
let backgroundImg;
let gameOverImg;
let stars = [];
let chance = 0.015;
let gameOver = false;
let displayMessage = true;
let messageStartTime;
let flapSound;
let americaSong;


function preload() {
  soundFormats('ogg', 'mp3');
  americaSong = loadSound("americaSong.mp3");
  flapSound = loadSound("flap.mp3");
  backgroundImg = loadImage("sky.jpg");
  eagleImg = loadImage("eagle.jpg");
  starImg = loadImage("star.jpg");
  gameOverImg = loadImage("gameOver.jpg");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  americaSong.play();
  americaSong.setVolume(0.03);
  eagle = new Eagle();
  messageStartTime = millis();
}

function keyPressed() {
  if (key === " " && !gameOver) {
    eagle.jump();
    flapSound.setVolume(0.5);
    flapSound.play();
  }

}

function draw() {
  if (gameOver) {
    gameOverImg.resize(950, 0);
    image(gameOverImg, width / 2 - gameOverImg.width / 2, height / 2 - gameOverImg.height / 2);
    return;
  }

  if (stars.length > 20) {
    chance = 0.02;
  }

  if (random(1) < chance) {
    stars.push(new Star());
  }

  background(backgroundImg);

  for (let i = stars.length - 1; i >= 0; i--) {
    let star = stars[i];
    star.move();
    star.show();

    if (eagle.hits(star)) {
      gameOver = true;
      americaSong.stop();
    }
  }

  eagle.show();
  eagle.move();

  if (millis() - messageStartTime < 6000) {
    textSize(map(sin(frameCount * 0.05), -1, 1, 90, 100));
    textFont("'Courier New'");
    fill(0);
    textAlign(CENTER, CENTER);
    text("Use space to play", width / 2, height / 2);
  }

}

class Eagle {
  constructor() {
    this.r = 150;
    this.x = 150;
    this.y = height - this.r;
    this.vy = 0.5;
    this.gravity = 0.5;
  }

  jump() {
    if (this.y >= 0) {
      this.vy = -10;
    }
  }

  show() {
    image(eagleImg, this.x, this.y, this.r, this.r);
  }

  move() {
    this.y += this.vy;
    this.vy += this.gravity;
    this.y = constrain(this.y, 0, height - this.r);
  }

  hits(star) {
    let d = dist(this.x + this.r / 2, this.y + this.r / 2, star.x + star.r / 2, star.y + star.r / 2);
    return d < (this.r / 2 + star.r / 2);
  }
}

class Star {
  constructor() {
    this.r = 90;
    this.x = width;
    this.y = random(0, height - this.r);
  }

  move() {
    this.x -= 8;
  }

  show() {
    image(starImg, this.x, this.y, this.r, this.r);
  }
}


