# MiniX02 - Melatonin
![hello](https://gitlab.com/matiasaestheticprogramming/aesthetic-programming/-/raw/main/MiniX02/Screenshot_2024-03-09_at_19.50.11.png?ref_type=heads)

[Click here to run the program](https://matiasaestheticprogramming.gitlab.io/aesthetic-programming/MiniX02)

[Click here to read the program](https://gitlab.com/matiasaestheticprogramming/aesthetic-programming/-/blob/main/MiniX02/sketch.js)

### What have I used and learnt? 

This time I have moved on from the fundamentals of p5.js and started to experiment with more complex code. I started by creating the geometrics to resemblance an emoji and coloring it. With my topic being melatonin I knew that I wanted more. And the effect of the eyes shaking and getting more and more red as time goes by was created. 

`````
New learning

  // Eyes
  let eye1X = 340 + random(-2, 2); // Add random offset to eye position

  // Eyes turning more and more red
  circleColorEyesRed -= 0.075;
  circleColorEyesGreen -= 0.25;
  circleColorEyesBlue -= 0.25;

  // Generate a random number between 1 and 12
  let hour = floor(random(1, 13));

  // Generate a random number to decide am/pm
  let amOrPm = random() < 0.5 ? "am" : "pm";
`````

### The Melatonin Emoji

Emojis are pictograms, a graphic symbol conveying meaning through their visual resemblance to a physical thing, like an eggplant emoji is a pictogram of the actual purple vegetable used for ratatouille. I could stop writing just here but, I chose not to, because it just isn’t that simple. When mentioning the eggplant emoji it is because of its multiple meanings varying in different social or cultural contexts. As a part of this, I remember the talk on understanding young people and their use of emojis, in an attempt to understand them better. Here it was truly clarified how emojis turned out to be more encrypted than Gen X and Boomers initially thought. Emojis are weird, and I think they always will be weird in the way they replace words. How one person “reads” the message behind an emoji can easily be misinterpreted by the receiver. 

This topic was one of the reasons for creating my MiniX02. I think that communication through text and the use of emojis is a grey area in trying to cognitively encrypt and understand the words behind what is received. So much of human communication is done non-verbally and through cues and the change of pitch, which is why I often find myself misunderstood or the opposite. In relation to this confusion in the use of digital language, we can draw connections to the confusion we subject the body to when we constantly expose our brain to blue light. The light from a screen consists of several color tones, and one of them is blue light. This particular color tone is interpreted by the brain as a form of daylight, thus keeping us awake. 

Blue light inhibits the production of melatonin, so we don't feel tired. One could say that the brain is tricked into believing it's daytime, and therefore it will try to keep you awake. We live in an era that is getting more and more digitalized, and although that necessarily isn’t a bad thing I want us to remember what real verbal communication is, I want us to understand each other. 

### References
* Sundhedsstyrelsen - https://www.sst.dk/da/teenageforaeldre/skaerm-rus-hjerne/teenagehjernen/hvordan-paavirkes-teenagehjernen-af-skaerm
* Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp 55-70
* Emojis Are Weird - https://www.youtube.com/watch?v=6Q4Rfk1fCJ4&ab_channel=KKlein

