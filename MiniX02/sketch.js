// Setting variables
let circleColorEyesRed = 250;
let circleColorEyesGreen = 250;
let circleColorEyesBlue = 250;
let background1 = [25, 25, 112, 50];

function setup() {
  resetSketch()
}

function resetSketch() {
  createCanvas(800, 800);
}

function draw() {
  background(background1);

  // Blue light
  strokeWeight(0);
  stroke(0);
  fill(0, 0, 255, 100);
  circle(700, 700, 350);
  filter(BLUR, 10);

  // Smiley
  fill(252, 226, 5);
  strokeWeight(5);
  stroke(250, 200, 5);
  circle(400, 400, 400);

  // Blue light
  strokeWeight(0);
  stroke(0);
  fill(0, 0, 200, 100);
  circle(700, 700, 350);

  // Eyes
  fill(circleColorEyesRed, circleColorEyesGreen, circleColorEyesBlue);
  stroke(0, 0, 0);
  let eye1X = 340 + random(-2, 2); // Add random offset to eye position
  let eye1Y = 350 + random(-2, 2);
  ellipse(eye1X, eye1Y, 80);

  let eye2X = 450 + random(-2, 2); // Add random offset to eye position
  let eye2Y = 350 + random(-2, 2);
  ellipse(eye2X, eye2Y, 80);

  // Inner eye
  fill(9);
  ellipse(350, 360, 20);
  ellipse(455, 360, 20);

  // Eyes turning more and more red
  circleColorEyesRed -= 0.075;
  circleColorEyesGreen -= 0.25;
  circleColorEyesBlue -= 0.25;
  
  // Generate a random number between 1 and 12
  let hour = floor(random(1, 13));

  // Generate a random number to decide am/pm
  let amOrPm = random() < 0.5 ? "am" : "pm";

  // Combine the hour and am/pm into a single string
  hourText = hour + " " + amOrPm;

  fill(0);
  textSize(50)
  text(hourText, 650, 60);

  // Eyes getting light again
  if (circleColorEyesRed < 100) {
    textSize(50)
    text("REFRESH TO TAKE A BREAK", 55, 350)
    text("REFRESH TO TAKE A BREAK", 55, 400)
    text("REFRESH TO TAKE A BREAK", 55, 450)
  }
}